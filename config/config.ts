export const getConfig = function (dirname) {
    return Conf(dirname);
};

const Conf = function (dirname) {
    return {

        // Server host
        host: 'localhost',

        // MIME types accepted
        acceptMimes: ["application/json"],

        mount: {
            '/api': `${dirname}/controllers/**/*.ts`
        },

        componentsScan: [
            `${dirname}/services/**/**.ts`,
            `${dirname}/middlewares/**/**.ts`
        ],

        // Logger settings
        logger: {
            debug: false,
            logRequest: true,
            requestFields: ["reqId", "method", "url", "headers", "query", "params", "duration"]
        },

        swagger: {
            path: "/api-docs"
        },

        // Database connection settings
        typeorm: [
            {
                name: "default",
                type: "postgres",
                host: 'ec2-54-246-101-215.eu-west-1.compute.amazonaws.com', //process.env['DATABASE_HOST'], //ec2-54-246-101-215.eu-west-1.compute.amazonaws.com
                port: 5432, //process.env['DATABASE_PORT'],
                username: 'uedtqqweknzvxd', //process.env['DATABASE_USERNAME'],
                password: '62a1aa84541af412d396f785b4105fdd146754ba23a54f2c24c6a1ef024369a0', // process.env['DATABASE_PASSWORD'],
                database: 'd8bspbjlvqdoil', //process.env['DATABASE_NAME'],
                entities: [
                    `${dirname}/persistance/postgres/entities/*.js`
                ],
                migrations: [
                    `${dirname}/persistance/postgres/migrations/*.js`
                ],
                extra: {
                    ssl: true
                },
                logging: true
            }
        ],

        salt: 'LwigmrmPIV', //process.env['SALT']
    }
};