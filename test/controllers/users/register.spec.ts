import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
import {bootstrap, Done, inject} from "@tsed/testing";
import {Server} from "../../../src/server";
import {RegisterCtrl} from "../../../src/controllers/users/register";
import {UserService} from "../../../src/services/user.service";
import {InjectorService} from "@tsed/common";
import {User} from "../../../src/persistance/postgres/entities/User.js";
chai.use(chaiAsPromised);
const expect = chai.expect;
const assert = chai.assert;

describe("Register Controller", () => {

    // create locals map
    const locals = new Map<any, any>();

    // bootstrap your Server to load all endpoints before run your test
    before(bootstrap(Server));

    before(function () {
        // replace DbService by a faker
        locals.set(UserService, {
            create: (user: User) => {
                if (user.phoneNumber === 666666666) throw new Error();
                return true;
            }
        });
    });

    it("should be instanceable", inject([InjectorService], (injector: InjectorService) => {
        // give the locals map to the invoke method
        const instance: RegisterCtrl = injector.invoke<RegisterCtrl>(RegisterCtrl, locals);

        // and test it
        expect(!!instance).to.be.true;
    }));

    it("a register should succeed if user doesn't exist", inject([InjectorService, Done], (injector: InjectorService, done: Done) => {
        const instance: RegisterCtrl = injector.invoke<RegisterCtrl>(RegisterCtrl, locals);
        instance.register(612345678, 1996, false)
            .then((result) => {
                expect(result.message).to.equal("User created successfully");
                done();
            })
            .catch(() => {
                assert.fail();
                done();
            });
    }));
});