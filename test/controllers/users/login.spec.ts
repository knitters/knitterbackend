import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
import {bootstrap, Done, inject} from "@tsed/testing";
import {Server} from "../../../src/server";
import {LoginCtrl} from "../../../src/controllers/users/login";
import {UserService} from "../../../src/services/user.service";
import {InjectorService} from "@tsed/common";
import {User} from "../../../src/persistance/postgres/entities/User.js";
import {InternalServerError, Unauthorized} from "ts-httpexceptions";
chai.use(chaiAsPromised);
const expect = chai.expect;
const assert = chai.assert;

describe("Login Controller", () => {

    // create locals map
    const locals = new Map<any, any>();

    // bootstrap your Server to load all endpoints before run your test
    before(bootstrap(Server));

    before(function () {
        // replace DbService by a faker
        locals.set(UserService, {
            findOne: ({ phoneNumber, hash }) => {
                return (phoneNumber === 666666666 && hash === "abcde" ? new User() : null);
            }
        });
    });

    it("should be instanceable", inject([InjectorService], (injector: InjectorService) => {
        // give the locals map to the invoke method
        const instance: LoginCtrl = injector.invoke<LoginCtrl>(LoginCtrl, locals);

        // and test it
        expect(!!instance).to.be.true;
    }));

    it("a login should succeed if user exists and password match", inject([InjectorService, Done], (injector: InjectorService, done: Done) => {
        const instance: LoginCtrl = injector.invoke<LoginCtrl>(LoginCtrl, locals);
        instance.login(666666666, "abcde")
            .then((result) => {
                expect(result.message).to.equal("Login successful");
                done();
            })
            .catch(() => {
                assert.fail();
                done();
            });
    }));

    it("a login should fail if user exists and password don't match", inject([InjectorService, Done], (injector: InjectorService, done: Done) => {
        const instance: LoginCtrl = injector.invoke<LoginCtrl>(LoginCtrl, locals);
        instance.login(666666666, "qwerty")
            .then((result) => {
                assert.fail();
                done();
            })
            .catch((err) => {
                expect(err.message).to.equal("Wrong username and password combination");
                done();
            });
    }));

    it("a login should fail if user doesn't exist", inject([InjectorService, Done], (injector: InjectorService, done: Done) => {
        const instance: LoginCtrl = injector.invoke<LoginCtrl>(LoginCtrl, locals);
        instance.login(612345678, "abcde")
            .then((result) => {
                assert.fail();
                done();
            })
            .catch((err) => {
                expect(err.message).to.equal("Wrong username and password combination");
                done();
            });
    }));
});