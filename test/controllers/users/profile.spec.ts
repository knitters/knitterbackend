import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
import {bootstrap, Done, inject} from "@tsed/testing";
import {Server} from "../../../src/server";
import {ProfileCtrl} from "../../../src/controllers/users/profile";
import {InjectorService} from "@tsed/common";
import {User} from "../../../src/persistance/postgres/entities/User.js";
chai.use(chaiAsPromised);
const expect = chai.expect;
const assert = chai.assert;

describe("Profile Controller", () => {

    // create locals map
    const locals = new Map<any, any>();

    // bootstrap your Server to load all endpoints before run your test
    before(bootstrap(Server));

    it("should be instanceable", inject([InjectorService], (injector: InjectorService) => {
        // give the locals map to the invoke method
        const instance: ProfileCtrl = injector.invoke<ProfileCtrl>(ProfileCtrl, locals);

        // and test it
        expect(!!instance).to.be.true;
    }));

    it("should return info from the logged user", inject([InjectorService, Done], (injector: InjectorService, done: Done) => {
        const instance: ProfileCtrl = injector.invoke<ProfileCtrl>(ProfileCtrl, locals);
        const logged_user = new User();
        logged_user.id = "1234-asdf-qwer";
        const promise = instance.getProfile({ user: logged_user });
        promise
            .then((result) => {
                expect(result.id).to.equal("1234-asdf-qwer");
                done()
            })
            .catch(() => {
                assert.fail();
                done();
            })

    }));
});