import {MigrationInterface, QueryRunner} from "typeorm";

export class deleteOfferDemandRequestNull1546621761728 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "offerRequests" ALTER COLUMN "deletedAt" DROP NOT NULL`);
      await queryRunner.query(`ALTER TABLE "demandRequests" ALTER COLUMN "deletedAt" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "offerRequests" ALTER COLUMN "deletedAt" TIMESTAMP NOT NULL`);
      await queryRunner.query(`ALTER TABLE "demandRequests" ALTER COLUMN "deletedAt" TIMESTAMP NOT NULL`);
    }

}
