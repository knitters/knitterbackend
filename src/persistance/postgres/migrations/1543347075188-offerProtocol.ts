import {MigrationInterface, QueryRunner} from "typeorm";

export class offerProtocol1543347075188 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" ADD "status" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "status"`);
    }

}
