import {MigrationInterface, QueryRunner} from "typeorm";

export class changeDemand1546557856919 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demands" DROP COLUMN "author"`);
        await queryRunner.query(`ALTER TABLE "demands" ADD "authorId" uuid`);
        await queryRunner.query(`ALTER TABLE "demands" ADD CONSTRAINT "FK_e857b6286e6ef86af28ca6e665c" FOREIGN KEY ("authorId") REFERENCES "users"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demands" DROP CONSTRAINT "FK_e857b6286e6ef86af28ca6e665c"`);
        await queryRunner.query(`ALTER TABLE "demands" DROP COLUMN "authorId"`);
        await queryRunner.query(`ALTER TABLE "demands" ADD "author" uuid NOT NULL`);
    }

}
