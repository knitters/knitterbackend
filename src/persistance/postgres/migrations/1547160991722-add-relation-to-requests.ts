import {MigrationInterface, QueryRunner} from "typeorm";

export class addRelationToRequests1547160991722 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offerRequests" DROP COLUMN "offer"`);
        await queryRunner.query(`ALTER TABLE "demandRequests" DROP COLUMN "demand"`);
        await queryRunner.query(`ALTER TABLE "offerRequests" ADD "offerId" uuid`);
        await queryRunner.query(`ALTER TABLE "demandRequests" ADD "demandId" uuid`);
        await queryRunner.query(`ALTER TABLE "offerRequests" ADD CONSTRAINT "FK_a22596a9b6865f04ffb0392c87c" FOREIGN KEY ("offerId") REFERENCES "offers"("id")`);
        await queryRunner.query(`ALTER TABLE "demandRequests" ADD CONSTRAINT "FK_b5e6093ca15ed2c1350767cdd8d" FOREIGN KEY ("demandId") REFERENCES "demands"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demandRequests" DROP CONSTRAINT "FK_b5e6093ca15ed2c1350767cdd8d"`);
        await queryRunner.query(`ALTER TABLE "offerRequests" DROP CONSTRAINT "FK_a22596a9b6865f04ffb0392c87c"`);
        await queryRunner.query(`ALTER TABLE "demandRequests" DROP COLUMN "demandId"`);
        await queryRunner.query(`ALTER TABLE "offerRequests" DROP COLUMN "offerId"`);
        await queryRunner.query(`ALTER TABLE "demandRequests" ADD "demand" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "offerRequests" ADD "offer" uuid NOT NULL`);
    }

}
