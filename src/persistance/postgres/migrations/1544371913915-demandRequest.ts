import {MigrationInterface, QueryRunner} from "typeorm";

export class demandRequest1544371913915 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demands" ADD "status" character varying `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demands" DROP COLUMN "status"`);
    }

}
