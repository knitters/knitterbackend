import {MigrationInterface, QueryRunner} from "typeorm";

export class Category1542133909149 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "nom" character varying, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "offers" ADD "category" uuid `);
        await queryRunner.query(`ALTER TABLE "demands" ADD "category" uuid `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "demands" DROP COLUMN "category"`);
        await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "category"`);
        await queryRunner.query(`DROP TABLE "category"`);
    }

}
