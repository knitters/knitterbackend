import {MigrationInterface, QueryRunner} from "typeorm";

export class offerRequest1543952026788 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "offerRequests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "description" character varying NOT NULL, "offer" uuid NOT NULL, "author" uuid NOT NULL, "status" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL, "updatedAt" TIMESTAMP NOT NULL, "deletedAt" TIMESTAMP, CONSTRAINT "PK_95873fee5091ac5e2b1148c5e9c" PRIMARY KEY ("id"))`);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.query(`DROP TABLE "offerRequests"`);
    }

}
