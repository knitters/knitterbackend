import {MigrationInterface, QueryRunner} from "typeorm";

export class demandRequest21544373406118 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "demandRequests" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "description" character varying NOT NULL, "demand" uuid NOT NULL, "author" uuid NOT NULL, "status" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL, "updatedAt" TIMESTAMP NOT NULL, "deletedAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_c4493ff5aeb855a5f5c290d891b" PRIMARY KEY ("id"))`);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "demandRequests"`);
    }

}
