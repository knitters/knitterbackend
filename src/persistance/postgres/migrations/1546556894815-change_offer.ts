import {MigrationInterface, QueryRunner} from "typeorm";

export class changeOffer1546556894815 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "author"`);
        await queryRunner.query(`ALTER TABLE "offers" ADD "authorId" uuid`);
        await queryRunner.query(`ALTER TABLE "offers" ADD CONSTRAINT "FK_3994a0e1665655337f0b874a44e" FOREIGN KEY ("authorId") REFERENCES "users"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" DROP CONSTRAINT "FK_3994a0e1665655337f0b874a44e"`);
        await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "authorId"`);
        await queryRunner.query(`ALTER TABLE "offers" ADD "author" uuid NOT NULL`);
    }

}
