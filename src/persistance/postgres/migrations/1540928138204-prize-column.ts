import {MigrationInterface, QueryRunner} from "typeorm";

export class prizeColumn1540928138204 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" ADD "prize" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "prize"`);
    }

}
