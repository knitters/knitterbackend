import {MigrationInterface, QueryRunner} from "typeorm";

export class User1539371483419 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "firstName" character varying, "lastName" character varying, "phoneNumber" integer NOT NULL UNIQUE, "hash" character varying NOT NULL, "imagePath" character varying, "description" character varying, "isElder" boolean NOT NULL, "dayOfBirth" smallint, "monthOfBirth" smallint, "yearOfBirth" smallint NOT NULL, "createdAt" TIMESTAMP NOT NULL, "updatedAt" TIMESTAMP NOT NULL, "deletedAt" TIMESTAMP, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
