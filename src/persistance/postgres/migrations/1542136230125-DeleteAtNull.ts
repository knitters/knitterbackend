import {MigrationInterface, QueryRunner} from "typeorm";

export class DeleteAtNull1542136230125 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "demands" ALTER COLUMN "deletedAt" DROP NOT NULL`);
      await queryRunner.query(`ALTER TABLE "offers" ALTER COLUMN "deletedAt" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`ALTER TABLE "demands" ALTER COLUMN "deletedAt" TIMESTAMP NOT NULL`);
      await queryRunner.query(`ALTER TABLE "offers" ALTER COLUMN "deletedAt" TIMESTAMP NOT NULL`);
    }

}
