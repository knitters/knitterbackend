import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {Demand} from "./Demand.js";

export enum DemandRequestStatuses {
  PENDING = 'Pending',
  ACCEPTED = 'Accepted',
  DENIED = 'Denied'
}

@Entity("demandRequests")
export class DemandRequest {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  description: string;

  @ManyToOne(type => Demand, demand => demand.requests)
  demand: Demand;

  @Column("uuid")
  author: string;

  @Column()
  status: DemandRequestStatuses;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @Column()
  deletedAt: Date;


}