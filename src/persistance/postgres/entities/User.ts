import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {Offer} from "./Offer.js";
import {Demand} from "./Demand.js";

@Entity("users")
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    phoneNumber: number;

    @Column()
    hash: string;

    @Column()
    imagePath: string;

    @Column()
    description: string;

    @Column()
    isElder: boolean;

    @Column()
    dayOfBirth: number;

    @Column()
    monthOfBirth: number;

    @Column()
    yearOfBirth: number;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

    @Column()
    deletedAt: Date;

    @OneToMany(type => Offer, offer => offer.author)
    offers: Offer[];

    @OneToMany(type => Demand, demand => demand.author)
    demands: Demand[];

    toJSON () {
        return {
            id: this.id,
            phoneNumber: this.phoneNumber,
            firstName: this.firstName,
            lastName: this.lastName,
            imagePath: this.imagePath,
            description: this.description,
            isElder: this.isElder
        }
    }

}