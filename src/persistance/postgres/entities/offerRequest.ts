import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {Demand} from "./Demand";
import {Offer} from "./Offer";

export enum OfferRequestStatuses {
    PENDING = 'Pending',
    ACCEPTED = 'Accepted',
    DENIED = 'Denied'
}

@Entity("offerRequests")
export class OfferRequest {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  description: string;

  @ManyToOne(type => Offer, offer => offer.requests)
  offer: Offer;

  @Column("uuid")
  author: string;

  @Column()
  status: OfferRequestStatuses;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @Column()
  deletedAt: Date;


}