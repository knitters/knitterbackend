import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {User} from "./User";
import {DemandRequest} from "./demandRequest";

export enum DemandStatuses {
    OPEN = "Open",
    DOING = "Doing",
    DONE = "Done"
}

@Entity("demands")
export class Demand {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @ManyToOne(type => User, user => user.offers)
  author: User;

  @Column("uuid")
  category: string;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @Column()
  deletedAt: Date;

  @Column()
  status: DemandStatuses;

  @OneToMany(type => DemandRequest, req => req.demand)
  requests: DemandRequest[];

}