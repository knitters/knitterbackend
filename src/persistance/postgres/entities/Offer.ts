import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {User} from "./User.js";
import {OfferRequest} from "./offerRequest";

export enum OfferStatuses {
    OPEN = "Open",
    DOING = "Doing",
    DONE = "Done"
}

@Entity("offers")
export class Offer {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    imagePath: string;

    @ManyToOne(type => User, user => user.offers)
    author: User;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

    @Column()
    deletedAt: Date;

    @Column()
    prize: string;

    @Column("uuid")
    category: string;

    @Column()
    status: OfferStatuses;

    @OneToMany(type => OfferRequest, req => req.offer)
    requests: OfferRequest[];

}