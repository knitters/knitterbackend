const aws = require('aws-sdk');
const express = require('express');
const multer = require('multer');
const multerS3 = require('multer-s3');
const crypto = require('crypto');
const Path = require('path');
const mime = require('mime');

aws.config.loadFromPath(`${__dirname}/../config/aws.config.json`);
const s3 = new aws.S3({});

const s3StorageStrategy = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'cloud-cube-eu',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            cb(null, `t4z7s3klbu3p/public/${Date.now().toString()}.${mime.getExtension(file.mimetype)}`)
        }
    })
});

const diskStorageStrategy = {
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, Path.resolve(__dirname) + '/uploads/')
        },
        filename: function (req, file, cb) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
            });
        }
    })
};

export default s3StorageStrategy;