import {
    Authenticated, BodyParams, Controller, Delete, Get, PathParams, Post, Put, QueryParams, Req, Required,
    RouteService
} from "@tsed/common";
import {Conflict, InternalServerError, NotFound} from "ts-httpexceptions";
import {Demand, DemandStatuses} from "../../persistance/postgres/entities/Demand";
import {LessThan, Like, MoreThan} from "typeorm";
import {DemandService} from "../../services/demand.service";
import {CategoryService} from "../../services/category.service";
import * as atob from 'atob';
import * as btoa from 'btoa';
import {DemandRequest, DemandRequestStatuses} from "../../persistance/postgres/entities/demandRequest";

@Controller("/demand")
export class DemandCtrl {

    private cache: Array<any> = null;
    private cacheSize: number = null;

    constructor(private routeService: RouteService, private demandService: DemandService, private categoryService: CategoryService) {

    }

    @Get('/')
    public async getDemands(@QueryParams("size") @Required size: number,
                            @QueryParams("filter") filter: string,
                            @QueryParams("category") category: string,
                            @QueryParams("author") author: string,
                            @QueryParams("status") status: DemandStatuses,
                            @QueryParams("next_cursor") next_cursor: string,
                            @QueryParams("prev_cursor") prev_cursor: string) {
        let demands;
        let isFirstPage = !next_cursor && !prev_cursor;
        const queryParams = {
            select: ['id', 'title', 'description', 'author', 'createdAt', 'status'],
            take: size + 1,
            order: {createdAt: 'DESC'},
            where: {deletedAt: null},
            relations: ['author']
        };
        if (filter) {
            queryParams.where['title'] = Like(`%${filter}%`);
        }
        if (category) {
            queryParams.where['category'] = category;
        }
        if (author) {
            queryParams.where['author'] = author;
        }
        if (status) {
            queryParams.where['status'] = status;
        }
        if (!isFirstPage) {
            // Check if previous cursor points to the first page
            if (prev_cursor) {
                const prevTimestamp = atob(prev_cursor);
                if (this.checkTimestampInCache(prevTimestamp)) {
                    isFirstPage = true;
                } else {
                    queryParams['order'] = {createdAt: 'ASC'};
                    queryParams['where']['createdAt'] = MoreThan(new Date(parseInt(prevTimestamp) - 1));
                }
            }

            if (next_cursor) {
                const nextTimestamp = atob(next_cursor);
                queryParams['where']['createdAt'] = LessThan(new Date(parseInt(nextTimestamp)));
            }
        }

        // If is first page, return from cache
        if (isFirstPage && this.cacheSize === size) {
            demands = this.cache;
        } else {
            // Otherwise, Fetch data
            demands = await this.demandService.find(queryParams);

            // If is first page, store in cache
            if (isFirstPage) {
                this.cache = demands;
                this.cacheSize = demands.length;
            }

            // Must reverse the result when fetching previous
            if (prev_cursor) {
                demands = demands.reverse();
            }
        }

        // If no info, error 404
        if (demands.length === 0) throw (new NotFound("No demands were found"));
        // Otherwise, send successful response
        let responseSize = Math.min(demands.length, size);
        let isLastPage = demands.length <= size;
        const metadata = {
            size: responseSize,
            prev_cursor: isFirstPage ? null : btoa(demands[0].createdAt.getTime()),
            next_cursor: isLastPage ? null : btoa(demands[demands.length - 2].createdAt.getTime()),
            isFirst: isFirstPage,
            isLast: isLastPage
        };
        return {
            demands: demands.slice(0, responseSize),
            metadata
        };
    }

    @Get("/:id")
    public async getOneDemand(@PathParams("id") id: string) {
        return await this.demandService.find({
            where: { id },
            relations: ["author"]
        });
    }

    @Post("/")
    @Authenticated()
    public async addDemand(@BodyParams("description") @Required description: string,
                           @BodyParams("title") @Required title: string,
                           @BodyParams("categoryId") @Required categoryId: string,
                           @Req() request) {
        try {
            const demand: Demand = new Demand();
            demand.title = title;
            demand.description = description;
            demand.author = request.user.id;
            demand.category = categoryId;
            demand.createdAt = new Date();
            demand.status = DemandStatuses.OPEN;
            demand.updatedAt = new Date();
            await this.demandService.create(demand);
            return {
                message: `Demand created successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Demand error: " + err));
        }
    }

    @Put("/")
    @Authenticated()
    public async updateDemand(@BodyParams("id") @Required id: string,
                              @BodyParams("title") title: string,
                              @BodyParams("description") description: string,
                              @BodyParams("categoryId") categoryId: string,
                              @Req() request) {
        try {
            const demand: Demand = new Demand();
            demand.id = id;
            demand.title = title || "";
            demand.description = description || "";
            demand.category = categoryId;
            demand.updatedAt = new Date();

            await this.demandService.update(demand);
            return {
                message: `Demand updated successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Demand error: " + err));
        }
    }

    @Delete("/")
    @Authenticated()
    public async deleteDemand(@BodyParams("id") @Required id: string) {
        try {
            await this.demandService.delete(id);
            return {
                message: `Demand deleted successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Demand error: " + err));
        }
    }

    //Abuelo
    @Post("/requestDemand")
    @Authenticated()
    public async requestDemand(@BodyParams("demandId") @Required demandId: string,
                               @BodyParams("description") @Required description: string,
                               @Req() request) {
        try {
              let demands = await this.demandService.findRequest({author: request.user.id, demand: demandId});
              if(demands.length != 0) {
                throw(new Conflict("You have requested this demand before"));
              }
              else {
                const demand = new Demand();
                demand.id = demandId;
                const demandRequest: DemandRequest = new DemandRequest();
                demandRequest.description = description;
                demandRequest.author = request.user.id;
                demandRequest.demand = demand;
                demandRequest.status = DemandRequestStatuses.PENDING;
                demandRequest.createdAt = new Date();
                demandRequest.updatedAt = new Date();
                await this.demandService.createRequest(demandRequest);
                return {
                  message: `Demand Request created successfully`
                };
              }
        } catch (err) {
            throw(new InternalServerError("DemandRequest error: " + err));
        }
    }



    //Interesado
    @Post("/getRequestsByDemand")
    @Authenticated()
    public async getRequestsByDemand(@BodyParams("demandId") @Required demandId: string,
                                     @Req() request) {
        try {

            return await this.demandService.findRequest({demand: demandId})
        } catch (err) {
            throw(new InternalServerError("DemandRequest error: " + err));
        }
    }

    //Interesado
    @Post("/acceptRequest")
    @Authenticated()
    public async acceptRequest(@BodyParams("requestId") @Required requestId: string,
                               @Req() request) {
        try {
            let demandRequests = await this.demandService.findRequest({id: requestId});
            let demandRequest = demandRequests[0];

            //Find others and change status to deny
            let otherDemandRequests = await this.demandService.findRequest({demand: demandRequest.demand});
            for (const demandReq of otherDemandRequests) {
                demandReq.status = DemandRequestStatuses.DENIED;
                await this.demandService.updateRequest(demandReq);
            }

            //Find demand and change status
            let demands = await this.demandService.find({id: demandRequest.demand});
            let demand = demands[0];
            demand.status = DemandStatuses.DOING;
            await this.demandService.update(demand);


            demandRequest.status = DemandRequestStatuses.ACCEPTED;
            await this.demandService.updateRequest(demandRequest);
            return {
                message: `Request accepted successfully`
            };
        } catch (err) {
            throw(new InternalServerError("acceptRequest error: " + err));
        }
    }

    //Interesado
    @Post("/denyRequest")
    @Authenticated()
    public async denyRequest(@BodyParams("requestId") @Required requestId: string,
                             @Req() request) {
        try {
            let demandRequests = await this.demandService.findRequest({id: requestId});
            let demandRequest = demandRequests[0];
            demandRequest.status = DemandRequestStatuses.DENIED;
            await this.demandService.updateRequest(demandRequest);
            return {
                message: `Request denied successfully`
            };
        } catch (err) {
            throw(new InternalServerError("acceptRequest error: " + err));
        }
    }

    //Interesado
    @Post("/finishDemand")
    @Authenticated()
    public async finishDemand(@BodyParams("demandId") @Required demandId: string,
                              @Req() request) {
        try {
            let demands = await this.demandService.find({id: demandId});
            let demand = demands[0];
            demand.status = DemandStatuses.DONE;
            await this.demandService.update(demand);
            return {
                message: `Demand done successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Demand error: " + err));
        }
    }

    private checkIdInCache(id: string) {
        if (!this.cache || !this.cacheSize) return false;
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].id === id) return true;
        }
        return false;
    }

    private checkTimestampInCache(timestamp: number) {
        if (!this.cache || !this.cacheSize) return false;
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].createdAt.getTime() <= timestamp) return true;
        }
        return false;
    }

    private async reloadCache() {
        try {
            const queryParams = {
                select: ['id', 'title', 'description', 'author', 'createdAt'],
                take: this.cacheSize,
                order: {createdAt: 'DESC'},
                where: {deletedAt: null}
            };

            // Fetch data
            const demands = await this.demandService.find(queryParams);
            this.cache = demands;
        } catch (err) {
            // Invalidate cache
            this.cache = null;
        }
    }

}