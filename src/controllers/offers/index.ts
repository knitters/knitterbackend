import {
    Authenticated, BodyParams, Controller, Delete, Get, PathParams, Post, Put, QueryParams, Req, Required,
    RouteService
} from "@tsed/common";
import {OfferService} from "../../services/offer.service";
import {Conflict, InternalServerError, NotFound} from "ts-httpexceptions";
import {Offer, OfferStatuses} from "../../persistance/postgres/entities/Offer.js";
import {MultipartFile} from "@tsed/multipartfiles";
import * as atob from 'atob';
import * as btoa from 'btoa';
import {LessThan, Like, MoreThan} from "typeorm";
import {OfferRequest, OfferRequestStatuses} from "../../persistance/postgres/entities/offerRequest";

@Controller("/offer")
export class OfferCtrl {

    private cache: Array<any> = null;
    private cacheSize: number = null;

    constructor(private routeService: RouteService, private offerService: OfferService) {

    }

    @Get('/')
    public async getOffers(@QueryParams("size") @Required size: number,
                           @QueryParams("filter") filter: string,
                           @QueryParams("category") category: string,
                           @QueryParams("author") author: string,
                           @QueryParams("status") status: OfferStatuses,
                           @QueryParams("next_cursor") next_cursor: string,
                           @QueryParams("prev_cursor") prev_cursor: string) {
        let offers;
        let isFirstPage = !next_cursor && !prev_cursor;
        const queryParams = {
            select: ['id', 'title', 'description', 'imagePath', 'author', 'createdAt', 'status'],
            take: size + 1,
            order: {createdAt: 'DESC'},
            where: {deletedAt: null},
            relations: ['author']
        };
        if (filter) {
            queryParams.where['title'] = Like(`%${filter}%`);
        }
        if (category) {
            queryParams.where['category'] = category;
        }
        if (author) {
            queryParams.where['author'] = author;
        }
        if (status) {
            queryParams.where['status'] = status;
        }
        if (!isFirstPage) {
            // Check if previous cursor points to the first page
            if (prev_cursor) {
                const prevTimestamp = atob(prev_cursor);
                if (this.checkTimestampInCache(prevTimestamp)) {
                    isFirstPage = true;
                } else {
                    queryParams['order'] = {createdAt: 'ASC'};
                    queryParams['where']['createdAt'] = MoreThan(new Date(parseInt(prevTimestamp) - 1));
                }
            }

            if (next_cursor) {
                const nextTimestamp = atob(next_cursor);
                queryParams['where']['createdAt'] = LessThan(new Date(parseInt(nextTimestamp)));
            }
        }

        // If is first page, return from cache
        if (isFirstPage && this.cacheSize === size) {
            offers = this.cache;
        } else {
            // Otherwise, Fetch data
            offers = await this.offerService.find(queryParams);

            // If is first page, store in cache
            if (isFirstPage) {
                this.cache = offers;
                this.cacheSize = offers.length;
            }

            // Must reverse the result when fetching previous
            if (prev_cursor) {
                offers = offers.reverse();
            }
        }

        // If no info, error 404
        if (offers.length === 0) throw (new NotFound("No offers were found"));
        // Otherwise, send successful response
        let responseSize = Math.min(offers.length, size);
        let isLastPage = offers.length <= size;
        const metadata = {
            size: responseSize,
            prev_cursor: isFirstPage ? null : btoa(offers[0].createdAt.getTime()),
            next_cursor: isLastPage ? null : btoa(offers[offers.length - 2].createdAt.getTime()),
            isFirst: isFirstPage,
            isLast: isLastPage
        };
        return {
            offers: offers.slice(0, responseSize),
            metadata
        };
    }

    @Get("/:id")
    public async getOneOffer(@PathParams("id") id: string) {
        return await this.offerService.find({
            where: { id },
            relations: ["author"]
        });
    }

    @Post("/")
    @Authenticated()
    public async addOffer(@BodyParams("description") @Required description: string,
                          @BodyParams("title") @Required title: string,
                          @MultipartFile('image') file: any,
                          @BodyParams("categoryId") @Required categoryId: string,
                          @Req() request) {
        try {
            const offer: Offer = new Offer();
            offer.title = title;
            offer.description = description;
            offer.imagePath = file ? file.location : "";
            offer.author = request.user.id;
            offer.status = OfferStatuses.OPEN;
            offer.category = categoryId;
            offer.createdAt = new Date();
            offer.updatedAt = new Date();
            await this.offerService.create(offer);
            if (this.cache && this.cacheSize > 0) {
                this.cache = [offer].concat(this.cache.pop());
            }
            return offer;
        } catch (err) {
            throw(new InternalServerError("Offer error: " + err));
        }
    }

    @Put("/")
    @Authenticated()
    public async updateOffer(@BodyParams("id") @Required id: string,
                             @BodyParams("title") title: string,
                             @BodyParams("description") description: string,
                             @MultipartFile('image') file: any,
                             @BodyParams("categoryId") categoryId: string,
                             @Req() request) {
        try {
            const offer: Offer = new Offer();
            offer.id = id;
            if (title) offer.title = title || "";
            if (description) offer.description = description || "";
            if (file) offer.imagePath = file.location || "";
            offer.updatedAt = new Date();
            offer.category = categoryId;
            await this.offerService.update(offer);
            return {
                message: `Offer updated successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Offer error: " + err));
        }
    }

    @Delete("/")
    @Authenticated()
    public async deleteOffer(@BodyParams("id") @Required id: string) {
        try {
            await this.offerService.delete(id);
            // Update cache
            if (this.checkIdInCache(id)) {
                this.reloadCache();
            }
            return {
                message: `Offer deleted successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Offer error: " + err));
        }
    }

    //Interesado
    @Post("/requestOffer")
    @Authenticated()
    public async requestOffer(@BodyParams("offerId") @Required offerId: string,
                              @BodyParams("description") @Required description: string,
                              @Req() request) {
        try {


            let offers = await this.offerService.findRequest({author: request.user.id, offer: offerId});
            if(offers.length != 0) {
                throw(new Conflict("You have requested this offer before"));
            }
            else {
              const offer = new Offer();
              offer.id = offerId;
              const offerRequest: OfferRequest = new OfferRequest();
              offerRequest.description = description;
              offerRequest.author = request.user.id;
              offerRequest.offer = offer;
              offerRequest.status = OfferRequestStatuses.PENDING;
              offerRequest.createdAt = new Date();
              offerRequest.updatedAt = new Date();
              offerRequest.deletedAt = null;
              await this.offerService.createRequest(offerRequest);
              return {
                message: `Offer Request created successfully`
              };
            }
        } catch (err) {
            throw(new InternalServerError("OfferRequest error: " + err));
        }
    }


    //Abuelo
    @Post("/getRequestsByOffer")
    @Authenticated()
    public async getRequestsByOffer(@BodyParams("offerId") @Required offerId: string,
                                    @Req() request) {
        try {
              let offers =  await this.offerService.find({
                    where: { id: offerId },
                    relations: ["author"]
                  });
              if(offers.length == 0) {
                throw(new Conflict("Offer does not exist"));
              }
              if(offers[0].author.id != request.user.id){
                throw(new Conflict("You're not allow to see other's requests"));
              }
            return await this.offerService.findRequest({offer: offerId})
        } catch (err) {
            throw(new InternalServerError("OfferRequest error: " + err));
        }
    }

    //Abuelo
    @Post("/acceptRequest")
    @Authenticated()
    public async acceptRequest(@BodyParams("requestId") @Required requestId: string,
                               @Req() request) {
        try {

            let offerRequests = await this.offerService.findRequest({id: requestId});
            let offerRequest = offerRequests[0];

            //Find others and change status to deny
            let otherOfferRequests = await this.offerService.findRequest({offer: offerRequest.offer});
            for (const offerReq of otherOfferRequests) {
                offerReq.status = OfferRequestStatuses.DENIED;
                await this.offerService.updateRequest(offerReq);
            }

            //Find offer and change status
            let offers = await this.offerService.find({id: offerRequest.offer});
            let offer = offers[0];
            offer.status = OfferStatuses.DOING;
            await this.offerService.update(offer);


            offerRequest.status = OfferRequestStatuses.ACCEPTED;
            await this.offerService.updateRequest(offerRequest);
            return {
                message: `Request accepted successfully`
            };
        } catch (err) {
            throw(new InternalServerError("acceptRequest error: " + err));
        }
    }

    //Abuelo
    @Post("/denyRequest")
    @Authenticated()
    public async denyRequest(@BodyParams("requestId") @Required requestId: string,
                             @Req() request) {
        try {
            let offerRequests = await this.offerService.findRequest({id: requestId});
            let offerRequest = offerRequests[0];
            offerRequest.status = OfferRequestStatuses.DENIED;
            await this.offerService.updateRequest(offerRequest);
            return {
                message: `Request denied successfully`
            };
        } catch (err) {
            throw(new InternalServerError("acceptRequest error: " + err));
        }
    }

    //Interesado
    @Post("/finishOffer")
    @Authenticated()
    public async finishOffer(@BodyParams("offerId") @Required offerId: string,
                             @Req() request) {
        try {
            let offers = await this.offerService.find({id: offerId});
            let offer = offers[0];
            offer.status = OfferStatuses.DONE;
            await this.offerService.update(offer);
            return {
                message: `Offer done successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Offer error: " + err));
        }
    }


    private checkIdInCache(id: string) {
        if (!this.cache || !this.cacheSize) return false;
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].id === id) return true;
        }
        return false;
    }

    private checkTimestampInCache(timestamp: number) {
        if (!this.cache || !this.cacheSize) return false;
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].createdAt.getTime() <= timestamp) return true;
        }
        return false;
    }

    private async reloadCache() {
        try {
            const queryParams = {
                select: ['id', 'title', 'description', 'imagePath', 'author', 'createdAt'],
                take: this.cacheSize,
                order: {createdAt: 'DESC'},
                where: {deletedAt: null}
            };

            // Fetch data
            const offers = await this.offerService.find(queryParams);
            this.cache = offers;
        } catch (err) {
            // Invalidate cache
            this.cache = null;
        }
    }

}