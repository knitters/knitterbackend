import {Authenticated, BodyParams, Controller, Delete, Get, Post, Put, Req, Required, RouteService} from "@tsed/common";
import {OfferService} from "../../services/offer.service";
import {InternalServerError} from "ts-httpexceptions";
import {User} from "../../persistance/postgres/entities/User.js";
import {Demand} from "../../persistance/postgres/entities/Demand";
import {Column, PrimaryGeneratedColumn} from "typeorm";
import {DemandService} from "../../services/demand.service";
import {Offer} from "../../persistance/postgres/entities/Offer.js";
import {CategoryService} from "../../services/category.service";
import {Category} from "../../persistance/postgres/entities/Category";

@Controller("/categories")
export class CategoriesCtrl {

  constructor(private routeService: RouteService, private categoryService: CategoryService) {

  }

  @Get('/')
  @Authenticated()
  public async getCategories () {
    return this.categoryService.findAll({});
  }



}