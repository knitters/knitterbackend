import Path = require("path");
import {Controller, Post, RouteService, BodyParams, Required} from "@tsed/common";
import {UserService} from "../../services/user.service";
import {User} from "../../persistance/postgres/entities/User.js";
import {InternalServerError} from "ts-httpexceptions";
import sha256 = require("sha256");
import {getConfig} from "../../../config/config";
const CONFIG = getConfig(Path.resolve(__dirname));

@Controller("/users/register")
export class RegisterCtrl {

    constructor(private routeService: RouteService, private userService: UserService) {

    }

    @Post("/")
    public async register(@BodyParams("phoneNumber") @Required phoneNumber: number,
                    @BodyParams("yearOfBirth") @Required yearOfBirth: number,
                    @BodyParams("isElder") @Required isElder: boolean) {
        try {
            const user: User = new User();
            user.phoneNumber = phoneNumber;
            user.hash = sha256(phoneNumber.toString() + CONFIG.salt);
            user.isElder = isElder;
            user.yearOfBirth = yearOfBirth;
            user.createdAt = new Date();
            user.updatedAt = new Date();
            await this.userService.create(user);
            return {
                message: `User created successfully`
            };
        } catch (err) {
            throw(new InternalServerError("Register error: " + err));
        }
    }
}