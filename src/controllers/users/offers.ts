import {
    Controller, RouteService, BodyParams, Required, Get, Authenticated, Req, PathParams
} from "@tsed/common";
import {Forbidden, InternalServerError} from "ts-httpexceptions";
import {OfferService} from "../../services/offer.service";

@Controller("/users/profile/offers")
export class UserOffersCtrl {

    constructor(private routeService: RouteService, private offerService: OfferService) {

    }

    @Get("/")
    @Authenticated()
    public async getUserOffers(@Req() request) {
        try {
            return await this.offerService.find({ author: request.user.id });
        } catch (err) {
            throw(new InternalServerError("Error: " + err));
        }
    }
}