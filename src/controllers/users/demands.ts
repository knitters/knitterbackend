import {
    Controller, RouteService, Get, Authenticated, Req, PathParams
} from "@tsed/common";
import {Forbidden, InternalServerError} from "ts-httpexceptions";
import {DemandService} from "../../services/demand.service";

@Controller("/users/profile/demands")
export class UserDemandsCtrl {

    constructor(private routeService: RouteService, private demandService: DemandService) {

    }

    @Get("/")
    @Authenticated()
    public async getUserDemands(@Req() request) {
        try {
            return await this.demandService.find({ author: request.user.id });
        } catch (err) {
            throw(new InternalServerError("Error: " + err));
        }
    }
}