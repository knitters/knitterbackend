import {Controller, Post, RouteService, BodyParams, Required, Authenticated} from "@tsed/common";
import {UserService} from "../../services/user.service";
import {InternalServerError, Unauthorized} from "ts-httpexceptions";
import sha256 = require("sha256");
import jwt = require("jsonwebtoken");
import fs = require("fs");

@Controller("/users/login")
export class LoginCtrl {

    constructor(private routeService: RouteService, private userService: UserService) {

    }

    @Post("/")
    public async login(@BodyParams("phoneNumber") @Required phoneNumber: number,
                       @BodyParams("hash") @Required hash: string) {
        try {
            const user = await this.userService.findOne({ phoneNumber, hash });
            if (user) {
                let disabledUser = (user.deletedAt !== null);
                if (disabledUser) {
                    user.deletedAt = null;
                    await this.userService.update(user);
                }
                const cert = fs.readFileSync(`${__dirname}/../../../keys/auth.key`);
                const token = jwt.sign(user.toJSON(), cert, { algorithm: 'RS256'});
                return {
                    token,
                    message: "Login successful",
                    wasDisabled: disabledUser
                };
            }
        } catch (err) {
            throw(new InternalServerError("Login error: " + err));
        }
        throw(new Unauthorized("Wrong username and password combination"));
    }
}