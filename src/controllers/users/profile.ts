import {Controller, RouteService, BodyParams, Required, Get, Authenticated, Req, Post, Put} from "@tsed/common";
import {UserService} from "../../services/user.service";
import {InternalServerError} from "ts-httpexceptions";
import {MultipartFile} from "@tsed/multipartfiles";

@Controller("/users/profile")
export class ProfileCtrl {

    constructor(private routeService: RouteService, private userService: UserService) {

    }

    @Get("/")
    @Authenticated()
    public async getProfile(@Req() request) {
      try {
        return await this.userService.findOne({ phoneNumber: request.user.phoneNumber });
      } catch (err) {
        throw(new InternalServerError("User error: " + err));
      }
    }

    @Put("/")
    @Authenticated()
    public async updateUser(@BodyParams("firstName") @Required firstName: string,
                            @BodyParams("lastName") @Required lastName: string,
                            @BodyParams("description") @Required description: string,
                            @MultipartFile('image') file: any,
                            @Req() request) {
        try {
          const user = await this.userService.findOne({ phoneNumber: request.user.phoneNumber });
          if(firstName) user.firstName = firstName;
          if(lastName) user.lastName = lastName;
          if(description) user.description = description;
          if(file) user.imagePath = file.location || "";
          user.updatedAt = new Date();
          await this.userService.update(user);
          return user;
        } catch (err) {
          throw(new InternalServerError("User error: " + err));
        }
    }
}