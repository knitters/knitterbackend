import {Controller, Post, RouteService, BodyParams, Required, Authenticated, Put, Req} from "@tsed/common";
import {UserService} from "../../services/user.service";
import {InternalServerError, Unauthorized} from "ts-httpexceptions";
import sha256 = require("sha256");
import jwt = require("jsonwebtoken");
import fs = require("fs");
import {User} from "../../persistance/postgres/entities/User.js";

@Controller("/users/disable")
export class DisableCtrl {

  constructor(private routeService: RouteService, private userService: UserService) {

  }


  @Post("/")
  @Authenticated()
  public async disableUser(@Req() request) {
    try {
      const user = await this.userService.findOne({ phoneNumber: request.user.phoneNumber });
      console.log(user);
      user.deletedAt = new Date();

      await this.userService.update(user);
      return {
        message: `User deleted successfully`
      };
    } catch (err) {
      throw(new InternalServerError("User error: " + err));
    }
  }
}