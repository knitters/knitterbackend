import {
    Authenticated, BodyParams, Controller, Delete, Get, Post, Put, QueryParams, Req, Required,
    RouteService
} from "@tsed/common";
import {UserService} from "../../services/user.service";
import {InternalServerError, NotFound} from "ts-httpexceptions";
import {User} from "../../persistance/postgres/entities/User.js";
import {getRepository, Like} from "typeorm";

@Controller("/users")
export class UserCtrl {

    constructor(private routeService: RouteService, private userService: UserService) {

    }

    @Get('/')
    @Authenticated()
    public async getUsers (@QueryParams("filter") filter: string) {
        let query = getRepository(User)
            .createQueryBuilder('user')
            .select(['user.id', 'user.firstName', 'user.lastName', 'user.imagePath', 'user.description', 'user.isElder', 'user.dayOfBirth', 'user.monthOfBirth', 'user.yearOfBirth']);
        if (filter) {
            query.where("LOWER(user.firstName) LIKE :filter", { filter: `%${filter.toLowerCase()}%` })
                .orWhere("LOWER(user.lastName) LIKE :filter", { filter: `%${filter.toLowerCase()}%` });
        }
        const result = await query.getMany();
        return {
            users: result,
            metadata: {
                size: result.length
            }
        };
    }

}