import {
    Authenticated, BodyParams, Controller, Delete, Get, PathParams, Post, Put, QueryParams, Req, Required,
    RouteService
} from "@tsed/common";
import {Conflict, InternalServerError, NotFound} from "ts-httpexceptions";
import {Demand, DemandStatuses} from "../../persistance/postgres/entities/Demand";
import {LessThan, Like, MoreThan} from "typeorm";
import {DemandService} from "../../services/demand.service";
import {CategoryService} from "../../services/category.service";
import * as atob from 'atob';
import * as btoa from 'btoa';
import {DemandRequest, DemandRequestStatuses} from "../../persistance/postgres/entities/demandRequest";
import {OfferService} from "../../services/offer.service";

@Controller("/xat")
export class XatCtrl {

  constructor(private routeService: RouteService, private demandService: DemandService, private offerService: OfferService) {

  }

  @Get("/getRequests")
  @Authenticated()
  public async getRequests(@Req() request) {
    try {
      //Oferta
      let offers = await this.offerService.find({author: request.user.id});
      let requests = []
      for (const offer of offers) {
        let offerRequests = await this.offerService.findRequest({
            where: {offer: offer.id},
            relations: ['offer']
        });
        for(const oR of offerRequests) {
          if(oR.status != 'Denied')
            requests.push(oR)
        }
      }
      let interesadoOfferRequests = await this.offerService.findRequest({
        where: {author: request.user.id},
          relations: ['offer']
    });
      requests = requests.concat(interesadoOfferRequests)

      //Demand

      let demands = await this.demandService.find({author: request.user.id});
      for (const demand of demands) {
        let demandRequests = await this.demandService.findRequest({
            where: {demand: demand.id},
            relations: ['demand']
        });
        for(const dR of demandRequests) {
          if(dR.status != 'Denied')
            requests.push(dR)
        }
      }
      let interesadoDemandRequests = await this.demandService.findRequest({
          where: {author: request.user.id},
          relations: ['demand']
      });
      requests = requests.concat(interesadoDemandRequests)


      return requests;
    } catch (err) {
      throw(new InternalServerError("OfferRequest error: " + err));
    }
  }
/*  Us faig un endpoint o els avis tinguin les requests de demandes acceptades(xat)
  i les requests d'ofertes que encara no ha acceptat/declinat(no xat) o que ha acceptat (xat)*/

  //Abuelo
/*  @Get("/getRequests")
  @Authenticated()
  public async getRequests(@Req() request) {
    try {
      let offers = await this.offerService.find({author: request.user.id});
      let requests = []
      for (const offer of offers) {
        console.log(offer)
        let offerRequest = await this.offerService.findRequest({offer: offer.id})
        console.log(offerRequest)
        requests = requests.concat(offerRequest)
      }
      return requests
    } catch (err) {
      throw(new InternalServerError("OfferRequest error: " + err));
    }
  }*/

  //Interesado
 /* @Get("/getRequestsDemand")
  @Authenticated()
  public async getRequestsDemand(@Req() request) {
    try {
      let demands = await this.demandService.find({author: request.user.id});
      let requests = [];
      for (const demand of demands) {
        console.log(demand);
        let demandRequest = await this.demandService.findRequest({demand: demand.id});
        console.log(demandRequest);
        requests = requests.concat(demandRequest)
      }
      return requests;
    } catch (err) {
      throw(new InternalServerError("DemandRequest error: " + err));
    }
  }*/



}