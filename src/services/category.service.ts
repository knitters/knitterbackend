import {Service, AfterRoutesInit} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection} from "typeorm";
import {getConfig} from "../../config/config";
import Path = require("path");
import {Category} from "../persistance/postgres/entities/Category";
import {Offer} from "../persistance/postgres/entities/Offer.js";
const CONFIG = getConfig(Path.resolve(__dirname));

@Service()
export class CategoryService implements AfterRoutesInit {
  private connection: Connection;
  constructor(private typeORMService: TypeORMService) {

  }

  $afterRoutesInit() {
    this.connection = this.typeORMService.get(CONFIG.typeorm['database']);
  }

  async findAll(params): Promise<Category[]> {
    return await this.connection.manager.find(Category, params);
  }

  async findOne(params): Promise<Category> {
    return await this.connection.manager.findOne(Category, params);
  }


}
