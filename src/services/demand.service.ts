import {Service, AfterRoutesInit} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection} from "typeorm";
import {getConfig} from "../../config/config";
import Path = require("path");
import {Demand} from "../persistance/postgres/entities/Demand";
import {Offer} from "../persistance/postgres/entities/Offer.js";
import {OfferRequest} from "../persistance/postgres/entities/offerRequest";
import {DemandRequest} from "../persistance/postgres/entities/demandRequest";
const CONFIG = getConfig(Path.resolve(__dirname));

@Service()
export class DemandService implements AfterRoutesInit {
  private connection: Connection;
  constructor(private typeORMService: TypeORMService) {

  }

  $afterRoutesInit() {
    this.connection = this.typeORMService.get(CONFIG.typeorm['database']);
  }


  async find(params): Promise<Demand[]> {
    return await this.connection.manager.find(Demand, params);
  }

  async create(demand: Demand): Promise<Demand> {
    try {
      return await this.connection.manager.save(demand);
    } catch (err) {
      throw err;
    }
  }

  async update(demand: Demand): Promise<Demand> {
    try {
      return await this.connection.manager.save(demand);
    } catch (err) {
      throw err;
    }
  }
  async delete(id: string): Promise<any> {
    try {
      return await this.connection.manager.delete(Demand, id);
    } catch (err) {
      throw err;
    }
  }
  async createRequest(demandRequest: DemandRequest): Promise<DemandRequest> {
    try {
      return await this.connection.manager.save(demandRequest);
    } catch (err) {
      throw err;
    }
  }

  async findRequest(params): Promise<DemandRequest[]> {
    return await this.connection.manager.find(DemandRequest, params);
  }

  async updateRequest(params): Promise<DemandRequest> {
    return await this.connection.manager.save(DemandRequest, params);
  }

}
