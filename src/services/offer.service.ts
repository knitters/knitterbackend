import {Service, AfterRoutesInit} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection} from "typeorm";
import {Offer} from "../persistance/postgres/entities/Offer";
import {getConfig} from "../../config/config";
import Path = require("path");
import {User} from "../persistance/postgres/entities/User.js";
import {Demand} from "../persistance/postgres/entities/Demand.js";
import {OfferRequest} from "../persistance/postgres/entities/offerRequest";
const CONFIG = getConfig(Path.resolve(__dirname));

@Service()
export class OfferService implements AfterRoutesInit {
    private connection: Connection;
    constructor(private typeORMService: TypeORMService) {

    }

    $afterRoutesInit() {
      this.connection = this.typeORMService.get(CONFIG.typeorm['database']);
    }

    async find(params): Promise<Offer[]> {
        return await this.connection.manager.find(Offer, params);
    }

    async create(offer: Offer): Promise<Offer> {
      try {
        return await this.connection.manager.save(offer);
      } catch (err) {
        throw err;
      }
    }

    async update(offer: Offer): Promise<Offer> {
      try {
        return await this.connection.manager.save(offer);
      } catch (err) {
        throw err;
      }
    }

    async delete(id: string): Promise<any> {
      try {
        return await this.connection.manager.delete(Offer, id);
      } catch (err) {
        throw err;
      }
    }

  async createRequest(offerRequest: OfferRequest): Promise<OfferRequest> {
    try {
      return await this.connection.manager.save(offerRequest);
    } catch (err) {
      throw err;
    }
  }

  async findRequest(params): Promise<OfferRequest[]> {
    return await this.connection.manager.find(OfferRequest, params);
  }

  async updateRequest(params): Promise<OfferRequest> {
    return await this.connection.manager.save(OfferRequest, params);
  }

}

