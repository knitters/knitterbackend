import {Service, AfterRoutesInit} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection} from "typeorm";
import {User} from "../persistance/postgres/entities/User.js";
import Path = require("path");
import {getConfig} from "../../config/config";
const CONFIG = getConfig(Path.resolve(__dirname));

@Service()
export class UserService implements AfterRoutesInit {
    private connection: Connection;
    constructor(private typeORMService: TypeORMService) {

    }

    $afterRoutesInit() {
        this.connection = this.typeORMService.get(CONFIG.typeorm['database']);
    }

    async create(user: User): Promise<User> {
        try {
            return await this.connection.manager.save(user);
        } catch (err) {
            throw err;
        }
    }

    async update(user: User): Promise<User> {
        try {
          return await this.connection.manager.save(user);
        } catch (err) {
          throw err;
        }
    }

    async findOne(params): Promise<User> {
        return await this.connection.manager.findOne(User, params);
    }
}
