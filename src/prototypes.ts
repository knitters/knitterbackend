const defaultObjectKeysMethod = Object.keys;
Object.keys = function (...params) {
    if (typeof params[0] === 'object' && params[0] === null) {
        return [];
    }
    return defaultObjectKeysMethod(...params);
};