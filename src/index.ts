import {$log} from "ts-log-debug";
import {Server} from "./server";
require('./prototypes');

$log.debug("Starting server...");
new Server().start().catch((er) => {
    $log.error(er);
});
