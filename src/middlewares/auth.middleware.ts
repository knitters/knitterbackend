import {
    AuthenticatedMiddleware, EndpointInfo, EndpointMetadata, HeaderParams, IMiddleware, Next, OverrideMiddleware,
    Req
} from "@tsed/common";
import {Forbidden, InternalServerError} from "ts-httpexceptions";
import jwt = require("jsonwebtoken");
import fs = require("fs");

@OverrideMiddleware(AuthenticatedMiddleware)
export class JWTAuthenticatedMiddleware implements IMiddleware {
    public use(@HeaderParams("Authorization") authentication: string,
               @EndpointInfo() endpoint: EndpointMetadata,
               @Req() request,
               @Next() next: Express.NextFunction) {

        const options = endpoint.get(AuthenticatedMiddleware) || {};

        if (!authentication) {
            throw new Forbidden("Must be an authenticated user to perform this action");
        }
        if (options.onlyElder) {
            throw new Forbidden("This user cannot perform this action");
        }

        try {
            const token = authentication.match(/Bearer (.+)/)[1];
            const cert = fs.readFileSync(`${__dirname}/../../keys/auth.key.pub`);
            jwt.verify(token, cert, { algorithm: 'RS256'}, function (err, decoded) {
                if (err) {
                    throw new Forbidden("Authentication failed: " + err);
                }
                request.user = decoded;
                next();
            });
        } catch (err) {
            throw new InternalServerError("Authentication error: " + err);
        }
    }
}
