import {ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware} from "@tsed/common";
import Path = require("path");
import "@tsed/swagger";
import {getConfig} from "../config/config";
import {$log} from "ts-log-debug";
import storageStrategy from './storage.strategy';
import "@tsed/multipartfiles";
import "@tsed/servestatic";

@ServerSettings(Object.assign({
    rootDir: Path.resolve(__dirname),
    port: process.env.PORT || 8080,
    httpsPort: false,
    multer: storageStrategy,
    serveStatic: {
        '/uploads': Path.resolve(__dirname) + '/uploads'
    }
}, getConfig(Path.resolve(__dirname))))
export class Server extends ServerLoader {

    /**
     * This method let you configure the middleware required by your application to works.
     * @returns {Server}
     */
    public $onMountingMiddlewares(): void|Promise<any> {
    
        const cookieParser = require('cookie-parser'),
            bodyParser = require('body-parser'),
            compress = require('compression'),
            methodOverride = require('method-override');

        this
            .use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }));

        return null;
    }

    $onReady() {
        $log.debug("Server initialized");
    }

    $onServerInitError(error): any {
        $log.error("Server encounter an error =>", error);
    }
}

